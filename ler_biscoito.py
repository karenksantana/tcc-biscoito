
import numpy as np
import cv2 as cv
import matplotlib.pyplot as plt
import json

def histograma(img, titulo):
	h = cv.calcHist([img], [0], None, [256], [0, 256]) 
	plt.figure()
	plt.title(titulo) 
	plt.xlabel("Intensidade") 
	plt.ylabel("Qtde de Pixels") 
	plt.plot(h)
	plt.xlim([-30, 256]) 

def read_dados_biscoito_bom():
	with open('pixel_biscoito_bom.json') as json_file:  
		data = json.load(json_file)
	return data
dadosBiscoitoBom = read_dados_biscoito_bom()
print(dadosBiscoitoBom)

video_capture = cv.VideoCapture(0)

while True:
	# Read each frame
	ret, frame = video_capture.read()
	# Convert frame to grey because cascading only works with greyscale image
	#print(frame)
	gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
	(thresh, blackAndWhiteImage) = cv.threshold(gray, 127, 255, cv.THRESH_BINARY)
	img_gaussian_blur = cv.GaussianBlur(blackAndWhiteImage, (5, 5), 0)
	_,img_binarizada_otsu = cv.threshold(img_gaussian_blur, 0, 255, cv.THRESH_BINARY+cv.THRESH_OTSU) 
	 
	mascara = np.zeros(frame.shape[:2], dtype = "uint8")
	gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
	unique, counts = np.unique(img_binarizada_otsu, return_counts=True)
	#print(unique)
	#print(counts)
	a = dict(zip(unique, counts))
	quantidade_pixel = {'branco':a[255], 'preto':a[0]}
	 
	print("pixel preto:" + str(quantidade_pixel['preto']))
	print("pixel branco:" + str(quantidade_pixel['branco']))
	print(dadosBiscoitoBom['branco']['min'])
	if(quantidade_pixel['branco'] >= dadosBiscoitoBom['branco']['min'] and quantidade_pixel['branco'] <= dadosBiscoitoBom['branco']['max']):
		print('biscoito bom')	
	else:
		print('biscoito ruim')	


	img_com_mascara = cv.bitwise_and(img_binarizada_otsu, img_binarizada_otsu, mask = mascara) 
	#cv.imshow("Gray", gray)3
	cv.imshow('img_binarizada_otsu', img_binarizada_otsu)
	histograma(img_binarizada_otsu, "histograma da img_binarizada_otsu")	
	plt.show()
	#cv.imshow("img", img_binarizada_otsu)
	if cv.waitKey(1) & 0xFF == ord('q'):
	 	break

video_capture.release()

cv.waitKey(0)
cv.destroyAllWindows()
